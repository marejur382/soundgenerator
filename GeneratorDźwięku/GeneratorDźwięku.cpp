// GeneratorDźwięku.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <cstdint>

const double fPI = 3.14159265359; // stała pi
const int iF1 = 125;	// pierwsza częstotliwość
const int iF2 = 560;	// druga częstotliwosć
const int iF3 = 834;	// trzecia częstotliwość
const int iFs = 44100;	// częstotliwość próbkowania (ilość próbek na sekunde)
const int iMaxAmplitude = 32760; // Maksymalna amplituda (głośność dźwięku)

void sinusGenerator(int _iF, int _iFs, float *&_sinusTab);
void sawtoothGenerator(int _iF, int _iFs, float *_sawtoothTab);

namespace little_endian_io
{
	template <typename Word>
	std::ostream& write_word(std::ostream& outs, Word value, unsigned size = sizeof(Word))
	{
		for (; size; --size, value >>= 8)
			outs.put(static_cast <char> (value & 0xFF));
		return outs;
	}
}
using namespace little_endian_io;

using namespace std;

int main()
{
	int iFSin1 = iF1;			// pierwsza częstotliwość sinusa
	int iFSin2 = 2 * iFSin1;	// druga częstotliwość sinusa
	int iFSin3 = 5 * iFSin1;	// trzecia częstotliwość sinusa

	float *fSin1 = new float[iFs]; //tablica dla pierwszego sinusa (częstotliwosć f)
	float *fSin2 = new float[iFs]; //tablica dla drugiego sinusa (częstotliwosć 2*f)
	float *fSin3 = new float[iFs];	//tablica dla trzeciego sinusa (częstotliwość 5*f)
	float *fSinSum = new float[iFs];	// tablica dla sumy sinusów
	float *fSawtooth = new float[iFs];	// tablica dla sygnału piłokształtnego
	short int *iOutput = new short[iFs];	// tablica na wyjście
	float fMaxSinAmp; // maksymalna amplituda
	float fMaxH; // maksymalny mnożnik (żeby sinus nie zgubił kształtu jeśli wyjdzie poza zakres (fMaxH = iMaxAmplitude/fMaxSinAmp)

	sinusGenerator(iFSin1, iFs, fSin1);
	sinusGenerator(iFSin2, iFs, fSin2);
	sinusGenerator(iFSin3, iFs, fSin3);

	for (int i = 0; i <= iFs; i++) {
		fSinSum[i] = fSin1[i] + fSin2[i] + fSin3[i];	// suma trzech sininusów
		if (i == 0 || fSinSum[i] >= fMaxSinAmp) {
			fMaxSinAmp = fSinSum[i];
		}
	}
	fMaxH = float(iMaxAmplitude / fMaxSinAmp);
	for (int i = 0; i <= iFs; i++) {
		fSinSum[i] = fSinSum[i] * fMaxH;
	}

	sawtoothGenerator(iFSin1, iFs, fSawtooth);

	for (int i = 0; i < iFs; i++) {
		iOutput[i] = fSinSum[i];
	}

	

	ofstream f("output.wav", ios::binary);
	// zapis pliku wav - nagłówki
	f << "RIFF----WAVEfmt ";     // RIFF
	write_word(f, 16, 4);  // no extension data
	write_word(f, 1, 2);  // PCM - integer samples
	write_word(f, 2, 2);  // dwa kanały, plik stereo
	write_word(f, iFs, 4);  // próbkowanie (ilość próbek na sekunde)
	write_word(f, (iFs * 2* sizeof(int) * 2 ) / 8, 4);  // (Sample Rate * BitsPerSample * Channels) / 8
	write_word(f, 4, 2);  // 2 inty i 2 kanały 
	write_word(f, 16, 2);  // liczba bitów na próbkę (2 bajty)

	size_t data_chunk_pos = f.tellp();
	f << "data----";  // dane

	for (int i = 0; i < iFs; i++) {
		write_word(f, (int)(iOutput[i]), 2);
		write_word(f, (int)(- iOutput[i]), 2);
	}
	size_t file_length = f.tellp();

	f.seekp(data_chunk_pos + 4);
	write_word(f, file_length - data_chunk_pos + 8);

	f.seekp(0 + 4);
	write_word(f, file_length - 8, 4);

	/*delete[] fSin1;
	delete[] fSin2;
	delete[] fSin3;
	delete[] fSinSum;
	delete[] fSawtooth;
	delete[] iOutput;*/

    return 0;
}


void sinusGenerator(int _iF, int _iFs, float *&_sinusTab) {
	float fN = _iFs / _iF;
	float fOmegaN;

	for (int i = 0; i <= _iFs; i++) {
		fOmegaN = ( i * fPI ) / fN;	// obliczenie pulsacji
		_sinusTab[i] = sin(fOmegaN);;
	}
}

void sawtoothGenerator(int _iF, int _iFs, float *_fSawtoothTab) {
	float fSample = 2;
	float fH = fSample / float(_iFs / _iF);

	for (int i = 0; i < _iFs; i++) {
		_fSawtoothTab[i] = (fSample - i * fH) - 1;
	}

}